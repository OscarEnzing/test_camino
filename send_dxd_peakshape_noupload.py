from os import listdir
from os.path import isfile, getctime, getsize
import time
import requests
import base64
import dwdatareader as dw
import numpy as np
import pandas as pd
from datetime import datetime
from dateutil import tz
from datetime import timedelta

URL = 'https://platform.proraildatalab.nl:7443/camino'
onlyfiles = [f for f in listdir() if '.dxd' in f]

date1 = '2018_08_26'
date2 = '2018_09_12'

# Between time1 and time 2
time1 = '0500'+'00'
time2 = '1000'+'00'

datetime_1 = datetime.strptime(date1+time1, '%Y_%m_%d%H%M%S')
datetime_2 = datetime.strptime(date2+time2, '%Y_%m_%d%H%M%S')
save_folder = 'PEAKSHAPE/'

# upload_files = ['Camino2_SLT_2018_09_05_134000.dxd', 'Camino2_SLT_2018_09_05_135000.dxd', 'Camino2_SLT_2018_09_05_140000.dxd', 'Camino2_SLT_2018_09_05_141000.dxd', 'Camino2_SLT_2018_09_05_142000.dxd', 'Camino2_SLT_2018_09_05_143000.dxd', 'Camino2_SLT_2018_09_05_144000.dxd', 'Camino2_SLT_2018_09_05_145000.dxd', 'Camino2_SLT_2018_09_05_145418.dxd', 'Camino2_SLT_2018_09_05_150000.dxd']

time_span = 0.002
total_data = 0
# Only look at data that was created in the last day, and the min at 5 sec makes sure the file is not still writing
for element in onlyfiles:
	if 601 < time.time()-getctime(element) < (24*3600):
	# if datetime_1.timestamp() < getctime(element) < datetime_2.timestamp():
		print(element)
		if total_data < 3e9:
			if getsize(element) > 500e3:
				channel_no_A = []
				channel_no_B = []
				dxd_data = dw.open(element)
				for i in dxd_data.values():
					no = str(i).split(' ')[1]
					if 'A-' in no:
						if "/PEAK" in no:
							channel_no_A.append(int(no[2]))
					if 'B-' in no:
						if "/PEAK" in no: 
							channel_no_B.append(int(no[2]))
							
				# Set timezone as the start time is set as UTC, while file name registers the local time
				to_zone = tz.gettz('Europe/Amsterdam')
				start_time = dxd_data.info.start_store_time.astimezone(to_zone)

				# Upload the PEAK data of A channels as seperate files
				for ch in channel_no_A:
					peaks = dxd_data['AI A-'+str(ch)+'/PEAK'].series()
					if all(peaks.iloc[1:] != peaks.iloc[0]):
						full_data = dxd_data['AI A-'+str(ch)].series()
						full_data.index = full_data.index.to_series().round(5)
						
						PEAK_times = peaks.index.to_series().round(5)
						for i, t in enumerate(PEAK_times):
							if i == 0:            
								s = full_data[t-(time_span/2):t+(time_span/2)]
																	   
							else:
								s_new = full_data[t-(time_span/2):t+(time_span/2)]
								s = pd.concat([s,s_new])
						s.index = s.index.to_series().map(
								lambda v: timedelta(seconds=v)+start_time)
								
						PEAKfilename = element.split('.')[0] + '_PEAKSHAPE_A'+str(ch)+'.csv'
						s.to_csv(save_folder+PEAKfilename)
						total_data += getsize(save_folder+PEAKfilename)
						
						dxd_file = open(save_folder+PEAKfilename, 'rb')
						dxd_file_binary = dxd_file.read()
						dxd_file_b64 = base64.b64encode(dxd_file_binary).decode()
						# Create basic json object
						json_payload = {'file': dxd_file_b64, 'recordingId': PEAKfilename} 

						 # Post file to server
						# requests.post(URL, json=json_payload)
					

						
				# Upload the PEAK data of B channels as seperate files
				for ch in channel_no_B:
					peaks = dxd_data['AI B-'+str(ch)+'/PEAK'].series()
					if all(peaks.iloc[1:] != peaks.iloc[0]):
						full_data = dxd_data['AI B-'+str(ch)].series()
						full_data.index = full_data.index.to_series().round(5)
						
						PEAK_times = peaks.index.to_series().round(5)
						for i, t in enumerate(PEAK_times):
							if i == 0:            
								s = full_data[t-(time_span/2):t+(time_span/2)]
																	   
							else:
								s_new = full_data[t-(time_span/2):t+(time_span/2)]
								s = pd.concat([s,s_new])
						s.index = s.index.to_series().map(
								lambda v: timedelta(seconds=v)+start_time)
								
						PEAKfilename = element.split('.')[0] + '_PEAKSHAPE_B'+str(ch)+'.csv'
						s.to_csv(save_folder+PEAKfilename)
						total_data += getsize(save_folder+PEAKfilename)

						dxd_file = open(save_folder+PEAKfilename, 'rb')
						dxd_file_binary = dxd_file.read()
						dxd_file_b64 = base64.b64encode(dxd_file_binary).decode()
						# Create basic json object
						json_payload = {'file': dxd_file_b64, 'recordingId': PEAKfilename} 

						 # Post file to server
						# requests.post(URL, json=json_payload)
					

				
				# # Upload the GPS data
				# GPS_x = dxd_data['Longitude'].series()/60
				# GPS_y = dxd_data['Latitude'].series()/60
				# GPS_x.index = GPS_x.index.to_series().map(
						# lambda v: timedelta(seconds=v)+start_time)
				# GPS_y.index = GPS_y.index.to_series().map(
						# lambda v: timedelta(seconds=v)+start_time)
				# df = pd.concat([GPS_x, GPS_y], axis = 1)

				# GPS_filename = element.split('.')[0] + '_GPS.csv'

				# df.to_csv(save_folder+GPS_filename)
				# total_data += getsize(save_folder+GPS_filename)
				# dxd_file = open(save_folder+GPS_filename, 'rb')
				# dxd_file_binary = dxd_file.read()
				# dxd_file_b64 = base64.b64encode(dxd_file_binary).decode()

				# # Create basic json object
				# json_payload = {'file': dxd_file_b64,'recordingId': GPS_filename} 

				 # Post file to server
				# requests.post(URL, json=json_payload)
				print(total_data)