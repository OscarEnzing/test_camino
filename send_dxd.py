from os import listdir
from os.path import isfile, getctime
import time
import requests
import base64

URL = 'https://platform.proraildatalab.nl:7443/camino'
onlyfiles = [f for f in listdir() if isfile(f)]

# Send data that was created in the last 5 minutes
for element in onlyfiles:
	if 5 < time.time()-getctime(element) < 306:
		dxd_file = open(element, 'rb')
		dxd_file_binary = dxd_file.read()
		dxd_file_b64 = base64.b64encode(dxd_file_binary).decode()


		
		# Create basic json object
		json_payload = {'file': dxd_file_b64,'recordingId': element} 


		 # Post file to server
		requests.post(URL, json=json_payload)