from os import listdir
from os.path import isfile, getctime, getsize
import time
import requests
import base64
import dwdatareader as dw
import numpy as np
import pandas as pd
from datetime import datetime
from dateutil import tz
from datetime import timedelta

URL = 'https://platform.proraildatalab.nl:7443/camino'
onlyfiles = [f for f in listdir() if '.dxd' in f]

date1 = '2018_09_13'
date2 = '2018_09_13'

# Between time1 and time 2
time1 = '0235'+'00'
time2 = '1020'+'00'

datetime_1 = datetime.strptime(date1+time1, '%Y_%m_%d%H%M%S')
datetime_2 = datetime.strptime(date2+time2, '%Y_%m_%d%H%M%S')
save_folder = 'PEAK_RMS/'
hadoop_folder_RMS = '/SLT_rms_as9_as10'
hadoop_folder_GPS = '/SLT_gps'
# hadoop_folder_PEAKS = 'SLT_peaks'?


total_data = 0
# Only look at data that was created in the last day, and the min at 5 sec makes sure the file is not still writing
for element in onlyfiles:
	# if 601 < time.time()-getctime(element) < (5*3600):
	if datetime_1.timestamp() < getctime(element) < datetime_2.timestamp():
		print(element)
		if total_data < 3e9:
			channel_no_A = []
			channel_no_B = []
			dxd_data = dw.open(element)
			for i in dxd_data.values():
				no = str(i).split(' ')[1]
				if 'A-' in no:
					if "/PEAK" in no:
						channel_no_A.append(int(no[2]))
				if 'B-' in no:
					if "/PEAK" in no: 
						channel_no_B.append(int(no[2]))
						
			# Set timezone as the start time is set as UTC, while file name registers the local time
			to_zone = tz.gettz('Europe/Amsterdam')
			start_time = dxd_data.info.start_store_time.astimezone(to_zone)

			# Upload the PEAK data of A channels as seperate files
			for i in channel_no_A:
				s = dxd_data['AI A-'+str(i)+'/PEAK'].series()
				if all(s.iloc[1:] != s.iloc[0]):
					s.index = s.index.to_series().map(
							lambda v: timedelta(seconds=v)+start_time)
					
					PEAKfilename = element.split('.')[0] + '_PEAKS_A'+str(i)+'.csv'
					s.to_csv(save_folder+PEAKfilename)
					total_data += getsize(save_folder+PEAKfilename)
					
					dxd_file = open(save_folder+PEAKfilename, 'rb')
					dxd_file_binary = dxd_file.read()
					dxd_file_b64 = base64.b64encode(dxd_file_binary).decode()
					# Create basic json object
					json_payload = {'file': dxd_file_b64, 'recordingId': PEAKfilename} 

					 # Post file to server
					requests.post(URL+hadoop_folder_PEAKS, json=json_payload)
				
				RMS = dxd_data['AI A-'+str(i)+'/RMS'].series()
				RMS.index = RMS.index.to_series().map(
						lambda v: timedelta(seconds=v)+start_time)
				
				RMSfilename = element.split('.')[0] + '_RMS_A'+str(i)+'.csv'
				RMS.to_csv(save_folder+RMSfilename)
				total_data += getsize(save_folder+RMSfilename)
				
				dxd_file = open(save_folder+RMSfilename, 'rb')
				dxd_file_binary = dxd_file.read()
				dxd_file_b64 = base64.b64encode(dxd_file_binary).decode()
				# Create basic json object
				json_payload = {'file': dxd_file_b64, 'recordingId': RMSfilename} 

				 # Post file to server
				requests.post(URL+hadoop_folder_RMS, json=json_payload)
					
			# Upload the PEAK data of B channels as seperate files
			for i in channel_no_B:
				s = dxd_data['AI B-'+str(i)+'/PEAK'].series()
				if all(s.iloc[1:] != s.iloc[0]):
					s.index = s.index.to_series().map(
							lambda v: timedelta(seconds=v)+start_time)
					
					PEAKfilename = element.split('.')[0] + '_PEAKS_B'+str(i)+'.csv'
					s.to_csv(save_folder+PEAKfilename)
					total_data += getsize(save_folder+PEAKfilename)

					dxd_file = open(save_folder+PEAKfilename, 'rb')
					dxd_file_binary = dxd_file.read()
					dxd_file_b64 = base64.b64encode(dxd_file_binary).decode()
					# Create basic json object
					json_payload = {'file': dxd_file_b64, 'recordingId': PEAKfilename} 

					 # Post file to server
					requests.post(URL+hadoop_folder_PEAKS, json=json_payload)
				
				RMS = dxd_data['AI B-'+str(i)+'/RMS'].series()
				RMS.index = RMS.index.to_series().map(
						lambda v: timedelta(seconds=v)+start_time)
				
				RMSfilename = element.split('.')[0] + '_RMS_B'+str(i)+'.csv'
				RMS.to_csv(save_folder+RMSfilename)
				total_data += getsize(save_folder+RMSfilename)
				
				dxd_file = open(save_folder+RMSfilename, 'rb')
				dxd_file_binary = dxd_file.read()
				dxd_file_b64 = base64.b64encode(dxd_file_binary).decode()
				# Create basic json object
				json_payload = {'file': dxd_file_b64, 'recordingId': RMSfilename} 

				 # Post file to server
				requests.post(URL+hadoop_folder_RMS, json=json_payload)
			
			# Upload the GPS data
			GPS_x = dxd_data['Longitude'].series()/60
			GPS_y = dxd_data['Latitude'].series()/60
			GPS_x.index = GPS_x.index.to_series().map(
					lambda v: timedelta(seconds=v)+start_time)
			GPS_y.index = GPS_y.index.to_series().map(
					lambda v: timedelta(seconds=v)+start_time)
			df = pd.concat([GPS_x, GPS_y], axis = 1)

			GPS_filename = element.split('.')[0] + '_GPS.csv'

			df.to_csv(save_folder+GPS_filename)
			total_data += getsize(save_folder+GPS_filename)
			dxd_file = open(save_folder+GPS_filename, 'rb')
			dxd_file_binary = dxd_file.read()
			dxd_file_b64 = base64.b64encode(dxd_file_binary).decode()

			# Create basic json object
			json_payload = {'file': dxd_file_b64,'recordingId': GPS_filename} 

			 # Post file to server
			requests.post(URL+hadoop_folder_GPS, json=json_payload)
			print(total_data)