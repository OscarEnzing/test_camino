from os import listdir
from os.path import isfile, getctime, getsize
import time
import requests
import base64
import time
from datetime import datetime
URL = 'https://platform.proraildatalab.nl:7443/camino'
onlyfiles = [f for f in listdir() if '.dxd' in f]

date1 = '2018_09_15'
date2 = '2018_09_15'

# Between time1 and time 2
time1 = '0500'+'00'
time2 = '1600'+'00'

datetime_1 = datetime.strptime(date1+time1, '%Y_%m_%d%H%M%S')
datetime_2 = datetime.strptime(date2+time2, '%Y_%m_%d%H%M%S')


total_data = 0
# print(total_data)
# time.sleep(5)
# Send data that was created in the last hour
for element in onlyfiles:
	if datetime_1.timestamp() < getctime(element) < datetime_2.timestamp():
	# if (10) < time.time()-getctime(element) < (7*3600):
		if total_data < 4e9:
			total_data += getsize(element)
			print(total_data)
			dxd_file = open(element, 'rb')
			dxd_file_binary = dxd_file.read()
			dxd_file_b64 = base64.b64encode(dxd_file_binary).decode()


			
			# Create basic json object
			json_payload = {'file': dxd_file_b64,'recordingId': element} 
			

			 # Post file to server
			requests.post(URL, json=json_payload)
			# r = requests.post(URL, json=json_payload)
			# print(r.status_code)
			
import smtplib

server = smtplib.SMTP('smtp.gmail.com', 587)
server.ehlo()
server.starttls()
server.ehlo()
server.login("camino.prorail@gmail.com", "caminoprorailns")

msg = "Data transfer complete, total data = " + str(total_data)
server.sendmail("camino.prorail@gmail.com", "camino.prorail@gmail.com", msg)
			