# functions

datatype = 'PEAK'
channel = 'A'

def create_file(datatype, channel_prefix, channel, csv_folder, URL, csv_folder, total_data)
	
	# Creates csv of the datatype in channel A
	
	s = dxd_data['AI '+channel+'-'+str(i)+'/'+datatype].series()
	if all(s.iloc[1:] != s.iloc[0]):
		s.index = s.index.to_series().map(
				lambda v: timedelta(seconds=v)+start_time)
		
		filename = element.split('.')[0] + '_'+datatype+'_'+channel+str(i)+'.csv'
		s.to_csv(csv_folder+filename)
		total_data += getsize(csv_folder+filename)
		return filename
	
def upload_file(
	dxd_file = open(csv_folder+filename, 'rb')
	dxd_file_binary = dxd_file.read()
	dxd_file_b64 = base64.b64encode(dxd_file_binary).decode()
	# Create basic json object
	json_payload = {'file': dxd_file_b64, 'recordingId': filename} 

	# Post file to server
	requests.post(URL, json=json_payload)