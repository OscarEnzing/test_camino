from os import listdir
from os.path import isfile, getctime, getsize
import time
import requests
import base64

# use: C:\ProgramData\Anaconda3\python.exe C:\DEWESoft\Data\send_dxd_test.py

URL = 'https://platform.proraildatalab.nl:7443/camino'
onlyfiles = [f for f in listdir() if isfile(f)]

test_file = 'Camino2_SLT_2018_09_04_124000.dxd'



dxd_file = open(test_file, 'rb')
dxd_file_binary = dxd_file.read()
dxd_file_b64 = base64.b64encode(dxd_file_binary).decode()



# Create basic json object
json_payload = {'file': dxd_file_b64,'recordingId': test_file} 
print(getsize(test_file))


 # Post file to server
r = requests.post(URL, json=json_payload)
print(r.status_code)

	

# Send data that was created in the last 5 minutes
# for element in onlyfiles:
	# if 5 < time.time()-getctime(element) < 306:
		# dxd_file = open(element, 'rb')
		# dxd_file_binary = dxd_file.read()
		# dxd_file_b64 = base64.b64encode(dxd_file_binary).decode()


		
		# # Create basic json object
		# json_payload = {'file': dxd_file_b64} 


		 # # Post file to server
		# requests.post(URL, json=json_payload)